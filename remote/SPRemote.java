/*
 * ServerPlus: remote
 * Copyright (c) Shankar Bhattarai <danger.bhattarai@gmail.com>
 */ 

package remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SPRemote extends Remote {
    int shutdownComputer() throws RemoteException;
    int restartComputer() throws RemoteException;
}
