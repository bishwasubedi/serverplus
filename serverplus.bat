@echo off
setlocal

set JAVA=java
set HOST=localhost

set SPTYPE=server
set SPCLASS=SPServer

set RMIREG=rmiregistry.exe

set SELF=%0
set BASE=%~dp0
set CP=%BASE:~0,-1%
set CB=%BASE:\=/%


:getopt
set OPT=%1
if "%OPT%"=="-c" goto setclient
if "%OPT%"=="-d" goto setdryrun
if "%OPT%"=="-h" goto sethost
goto setopt

:setclient
shift
set SPTYPE=client
set SPCLASS=SPClient
goto getopt

:setdryrun
shift
set DRYRUN=--dry-run
goto getopt

:sethost
shift
set HOST=%1
shift
goto getopt


:setopt

set SP=%SPTYPE%.policy
set RMI=-Djava.rmi.server.codebase=file:%CB%
set OPTS=-Djava.security.policy=%SP% %SPTYPE%.%SPCLASS%

goto %SPTYPE%

:client
rem tasklist /FI "IMAGENAME eq %RMIREG%" | find /I "%RMIREG%" > nul
rem if ERRORLEVEL 1 start %RMIREG%
set RMI=%RMI% -Djava.rmi.server.hostname=%HOST%
%JAVA% -cp %CP% %RMI% %OPTS% %DRYRUN%
goto done

:server
set CMD=%1
if "%CMD%"=="" goto usage
%JAVA% -cp %CP% %RMI% %OPTS% %HOST% %CMD%
goto done

:usage
echo.
echo Usage: %SELF% [-h client-host-name] Command.
echo.
echo   Commands:
echo     1 = Shutdown Computer.
echo     2 = Restart Computer.
echo.

:done
endlocal
