/*
 * ServerPlus: client
 * Copyright (c) Shankar Bhattarai <danger.bhattarai@gmail.com>
 */ 

package client;

//import java.lang.System;
//import java.lang.Runtime;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import remote.SPRemote;

public class SPClient implements SPRemote {

	private boolean dryrun;

	private static final String CMDLINE = "%s\\system32\\shutdown.exe %s -t 5";

    public SPClient() {
        super();
        dryrun = false;
    }

    public SPClient(boolean dr) {
        super();
        dryrun = dr;
    }

	public int shutdownComputer() {
		return execCmd("-s");
	}

	public int restartComputer() {
		return execCmd("-r");
	}

    public static void main(String[] args) {
        
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        
        try {
        	//special exception handler for registry creation
            LocateRegistry.createRegistry(1099); 
            System.out.println("java RMI registry created.");
        } catch (RemoteException e) {
            //do nothing, error means registry already exists
            System.out.println("java RMI registry already exists.");
        }
        
        try {
            boolean dr;
            if ((args.length > 0) && args[0].equals("--dry-run"))
            	dr = true;
            else
            	dr = false;
            SPRemote spclient = new SPClient(dr);
            SPRemote stub = (SPRemote) UnicastRemoteObject.exportObject(spclient, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind("SPRemote", stub);
            System.out.println("ServerPlus Client Started.");
        } catch (Exception e) {
            System.err.println("ServerPlus Client exception:");
            e.printStackTrace();
        }
    }

	private int execCmd(String cmd) {
		try {
			String sr = System.getenv("SystemRoot");
			String cmdline = String.format(CMDLINE, sr, cmd);
			if (!dryrun) {
				Runtime rt = Runtime.getRuntime();
				Process pr = rt.exec(cmdline);
				return pr.waitFor();
			}
			else {
				System.out.println("Command: " + cmdline);
				return 0;
			}
		}
		catch (Exception e) {
			System.err.println("ServerPlus Client exception:");
            e.printStackTrace();
			return 255;
		}
	}

}
