/*
 * ServerPlus: server
 * Copyright (c) Shankar Bhattarai <danger.bhattarai@gmail.com>
 */ 

package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import remote.SPRemote;

public class SPServer {
    
    public static void main(String args[]) {

        try {
            
            SPRemote spremote = SPServer.getRemote(args[0]);

			int cmd, ret;
			
			cmd = Integer.parseInt(args[1]);
			
			if (cmd == 1) {
				ret = spremote.shutdownComputer();
				if (ret == 0)
					System.out.println("Shutdown Success.");
				else
					System.out.println("Shutdown Failled.");
			}
			else if (cmd == 2) {
				ret = spremote.restartComputer();
				if (ret == 0)
					System.out.println("Restart successfull.");
				else
					System.out.println("Restart failled.");
			}
			else {
				System.out.println("SPServer: Unknown Command.");
			}
        } catch (Exception e) {
            System.err.println("SPServer exception:");
            e.printStackTrace();
        }

    }    

    public static SPRemote getRemote(String host) throws Exception {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        String name = "SPRemote";
        Registry registry = LocateRegistry.getRegistry(host);
        SPRemote spremote = (SPRemote) registry.lookup(name);
        return spremote;
    }    

}
